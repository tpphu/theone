package vn.com.tpbfico.external.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor(staticName = "getInstance")
public class UnitGroup  {
    private int id;
    private String name;
    private String description;
    private String code;
}
