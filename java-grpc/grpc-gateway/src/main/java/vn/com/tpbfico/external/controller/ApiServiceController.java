package vn.com.tpbfico.external.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import vn.com.tpbfico.external.dto.Ping;
import vn.com.tpbfico.external.dto.Pong;
import vn.com.tpbfico.external.service.PingService;

@RestController
@Slf4j
public class ApiServiceController {


    @Autowired
    PingService pingService;

    @PostMapping(path = "/api/v1/ping")
    public ResponseEntity<Pong> getPing(@RequestBody Ping request) {
//        Pong pong = Pong.getInstance();
//        pong.setTimestamp(System.currentTimeMillis());
//        pong.setMessage("success");
        log.info("request ======== " + request.toString());
        Pong pong = pingService.getPing(request);
        log.info("request ======== " + pong.toString());
        return new ResponseEntity<>(pong,HttpStatus.OK);
    }
}
