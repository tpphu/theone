package vn.com.tpbfico.external.utilities;

public class StringUtil {
    public static boolean isEmpty(Object object) {
        return object == null || object.equals("");
    }

    public static boolean hasText(Object object) {
        return !object.equals("");
    }
}
