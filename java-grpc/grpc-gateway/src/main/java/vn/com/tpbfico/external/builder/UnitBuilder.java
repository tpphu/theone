package vn.com.tpbfico.external.builder;

import lombok.Data;
import lombok.NoArgsConstructor;
import vn.com.tpbfico.external.dto.Unit;
import vn.com.tpbfico.external.dto.UnitGroup;


@Data
@NoArgsConstructor(staticName = "getInstance")
public class UnitBuilder {

    public Unit toUnitResponse(vn.com.tpbfico.grpc.Unit unit) {
        UnitGroup ug = UnitGroup.builder()
                .code(unit.getUnitGroup().getCode())
                .description(unit.getUnitGroup().getDescription())
                .name(unit.getUnitGroup().getName())
                .id(unit.getId())
                .build();
        return Unit.builder()
                .code(unit.getCode())
                .isActive(unit.getIsActive())
                .description(unit.getDescription())
                .editedBy(unit.getEditedBy())
                .id(unit.getId())
                .name(unit.getName())
                .unitGroupId(unit.getUnitGroup().getId())
                .unitGroup(ug)
                .build();
    }
}
