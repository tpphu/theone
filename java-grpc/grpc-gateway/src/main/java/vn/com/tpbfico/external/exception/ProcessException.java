package vn.com.tpbfico.external.exception;

import lombok.Data;
import vn.com.tpbfico.external.dto.Status;

@Data
public class ProcessException extends RuntimeException {

    private Status status;

    public ProcessException(Status status) {
        super(status.getMessage());
        this.status = status;
    }

}
