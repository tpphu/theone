package vn.com.tpbfico.external.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import vn.com.tpbfico.external.dto.Status;
import vn.com.tpbfico.external.exception.ProcessException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Slf4j
public class JwtAuthenticationFilter extends OncePerRequestFilter {


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String jwt = getJwtFromRequest(request);
        assert jwt != null;
        if (jwt.equals("helloworld")) { //todo code auth all service this block code
            filterChain.doFilter(request, response);
        } else {
            Status status = Status.builder()
                    .code(HttpStatus.UNAUTHORIZED.value())
                    .message("cant auth this request, please try again")
                    .build();
            throw new ProcessException(status);
        }
    }

    private String getJwtFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
        return null;
    }
}
