package vn.com.tpbfico.external.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor(staticName = "getInstance")
@Builder
public class Unit {
    private int id;
    private String code, name, description,editedBy;
    private boolean isActive;
    private int unitGroupId;
    private UnitGroup unitGroup;
}
