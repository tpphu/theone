package vn.com.tpbfico.external.service;

import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;
import vn.com.tpbfico.external.dto.Ping;
import vn.com.tpbfico.external.dto.Pong;
import vn.com.tpbfico.grpc.ApiServiceGrpc;
import vn.com.tpbfico.grpc.PingRequest;
import vn.com.tpbfico.grpc.PingResponse;

@Service
public class PingService {

    @GrpcClient("grpc-service")
    private ApiServiceGrpc.ApiServiceBlockingStub blockingStub;

    public Pong getPing(Ping ping) {
        PingRequest pingRequest = PingRequest.newBuilder()
                .setTimestamp(ping.getTimestamp())
                .build();
        PingResponse pingResponse = blockingStub.getPing(pingRequest);
        Pong pong = Pong.getInstance();
        pong.setMessage(pingResponse.getMessage());
        pong.setTimestamp(pingResponse.getTimestamp());
        return pong;
    }
}
