package vn.com.tpbfico.external.service;

import io.grpc.StatusRuntimeException;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;
import vn.com.tpbfico.external.builder.UnitBuilder;
import vn.com.tpbfico.external.dto.Unit;
import vn.com.tpbfico.grpc.GetUnitRequest;
import vn.com.tpbfico.grpc.UnitServiceGrpc;

@Service
public class UnitService {

    @GrpcClient("grpc-service")
    private UnitServiceGrpc.UnitServiceBlockingStub blockingStub;

    public Unit getUnit(int id) throws StatusRuntimeException {
        GetUnitRequest request = GetUnitRequest.newBuilder()
                .setId(id)
                .build();
        vn.com.tpbfico.grpc.Unit response = this.blockingStub.getUnit(request);
        return UnitBuilder.getInstance().toUnitResponse(response);
    }

}
