// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: common/common.proto

package vn.com.tpbfico.grpc.common;

public interface PaginationOrBuilder extends
    // @@protoc_insertion_point(interface_extends:common.Pagination)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>uint32 limit = 1;</code>
   */
  int getLimit();

  /**
   * <code>uint32 page = 2;</code>
   */
  int getPage();
}
