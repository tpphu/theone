// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: common/common.proto

package vn.com.tpbfico.grpc.common;

public final class Common {
  private Common() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_common_Pagination_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_common_Pagination_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_common_Filter_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_common_Filter_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_common_Sort_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_common_Sort_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_common_ListUInt32_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_common_ListUInt32_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_common_ListRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_common_ListRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_common_GetBaseRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_common_GetBaseRequest_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\023common/common.proto\022\006common\")\n\nPaginat" +
      "ion\022\r\n\005limit\030\001 \001(\r\022\014\n\004page\030\002 \001(\r\"\234\001\n\006Fil" +
      "ter\022\013\n\003key\030\001 \001(\t\022\016\n\006method\030\002 \001(\t\022\023\n\tint6" +
      "4_val\030\003 \001(\003H\000\022\024\n\nstring_val\030\004 \001(\tH\000\022\022\n\010b" +
      "ool_val\030\005 \001(\010H\000\022-\n\017list_uint32_val\030\006 \001(\013" +
      "2\022.common.ListUInt32H\000B\007\n\005value\"#\n\004Sort\022" +
      "\013\n\003key\030\001 \001(\t\022\016\n\006is_asc\030\002 \001(\010\"\033\n\nListUInt" +
      "32\022\r\n\005value\030\001 \003(\r\"}\n\013ListRequest\022\032\n\004sort" +
      "\030\001 \001(\0132\014.common.Sort\022\037\n\007filters\030\002 \003(\0132\016." +
      "common.Filter\022\t\n\001q\030\003 \001(\t\022&\n\npagination\030\004" +
      " \001(\0132\022.common.Pagination\"\034\n\016GetBaseReque" +
      "st\022\n\n\002id\030\001 \001(\r*s\n\tInputType\022\024\n\020TYPE_UNSP" +
      "ECIFIED\020\000\022\017\n\013TYPE_NUMBER\020\001\022\017\n\013TYPE_STRIN" +
      "G\020\002\022\r\n\tTYPE_LIST\020\003\022\014\n\010TYPE_BUY\020\004\022\021\n\rTYPE" +
      "_STANDARD\020\005B\036\n\032vn.com.tpbfico.grpc.commo" +
      "nP\001b\006proto3"
    };
    com.google.protobuf.Descriptors.FileDescriptor.InternalDescriptorAssigner assigner =
        new com.google.protobuf.Descriptors.FileDescriptor.    InternalDescriptorAssigner() {
          public com.google.protobuf.ExtensionRegistry assignDescriptors(
              com.google.protobuf.Descriptors.FileDescriptor root) {
            descriptor = root;
            return null;
          }
        };
    com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        }, assigner);
    internal_static_common_Pagination_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_common_Pagination_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_common_Pagination_descriptor,
        new java.lang.String[] { "Limit", "Page", });
    internal_static_common_Filter_descriptor =
      getDescriptor().getMessageTypes().get(1);
    internal_static_common_Filter_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_common_Filter_descriptor,
        new java.lang.String[] { "Key", "Method", "Int64Val", "StringVal", "BoolVal", "ListUint32Val", "Value", });
    internal_static_common_Sort_descriptor =
      getDescriptor().getMessageTypes().get(2);
    internal_static_common_Sort_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_common_Sort_descriptor,
        new java.lang.String[] { "Key", "IsAsc", });
    internal_static_common_ListUInt32_descriptor =
      getDescriptor().getMessageTypes().get(3);
    internal_static_common_ListUInt32_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_common_ListUInt32_descriptor,
        new java.lang.String[] { "Value", });
    internal_static_common_ListRequest_descriptor =
      getDescriptor().getMessageTypes().get(4);
    internal_static_common_ListRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_common_ListRequest_descriptor,
        new java.lang.String[] { "Sort", "Filters", "Q", "Pagination", });
    internal_static_common_GetBaseRequest_descriptor =
      getDescriptor().getMessageTypes().get(5);
    internal_static_common_GetBaseRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_common_GetBaseRequest_descriptor,
        new java.lang.String[] { "Id", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
