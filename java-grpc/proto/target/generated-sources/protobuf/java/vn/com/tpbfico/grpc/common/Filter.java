// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: common/common.proto

package vn.com.tpbfico.grpc.common;

/**
 * Protobuf type {@code common.Filter}
 */
public  final class Filter extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:common.Filter)
    FilterOrBuilder {
private static final long serialVersionUID = 0L;
  // Use Filter.newBuilder() to construct.
  private Filter(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private Filter() {
    key_ = "";
    method_ = "";
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return this.unknownFields;
  }
  private Filter(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    this();
    if (extensionRegistry == null) {
      throw new java.lang.NullPointerException();
    }
    int mutable_bitField0_ = 0;
    com.google.protobuf.UnknownFieldSet.Builder unknownFields =
        com.google.protobuf.UnknownFieldSet.newBuilder();
    try {
      boolean done = false;
      while (!done) {
        int tag = input.readTag();
        switch (tag) {
          case 0:
            done = true;
            break;
          case 10: {
            java.lang.String s = input.readStringRequireUtf8();

            key_ = s;
            break;
          }
          case 18: {
            java.lang.String s = input.readStringRequireUtf8();

            method_ = s;
            break;
          }
          case 24: {
            valueCase_ = 3;
            value_ = input.readInt64();
            break;
          }
          case 34: {
            java.lang.String s = input.readStringRequireUtf8();
            valueCase_ = 4;
            value_ = s;
            break;
          }
          case 40: {
            valueCase_ = 5;
            value_ = input.readBool();
            break;
          }
          case 50: {
            vn.com.tpbfico.grpc.common.ListUInt32.Builder subBuilder = null;
            if (valueCase_ == 6) {
              subBuilder = ((vn.com.tpbfico.grpc.common.ListUInt32) value_).toBuilder();
            }
            value_ =
                input.readMessage(vn.com.tpbfico.grpc.common.ListUInt32.parser(), extensionRegistry);
            if (subBuilder != null) {
              subBuilder.mergeFrom((vn.com.tpbfico.grpc.common.ListUInt32) value_);
              value_ = subBuilder.buildPartial();
            }
            valueCase_ = 6;
            break;
          }
          default: {
            if (!parseUnknownFieldProto3(
                input, unknownFields, extensionRegistry, tag)) {
              done = true;
            }
            break;
          }
        }
      }
    } catch (com.google.protobuf.InvalidProtocolBufferException e) {
      throw e.setUnfinishedMessage(this);
    } catch (java.io.IOException e) {
      throw new com.google.protobuf.InvalidProtocolBufferException(
          e).setUnfinishedMessage(this);
    } finally {
      this.unknownFields = unknownFields.build();
      makeExtensionsImmutable();
    }
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return vn.com.tpbfico.grpc.common.Common.internal_static_common_Filter_descriptor;
  }

  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return vn.com.tpbfico.grpc.common.Common.internal_static_common_Filter_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            vn.com.tpbfico.grpc.common.Filter.class, vn.com.tpbfico.grpc.common.Filter.Builder.class);
  }

  private int valueCase_ = 0;
  private java.lang.Object value_;
  public enum ValueCase
      implements com.google.protobuf.Internal.EnumLite {
    INT64_VAL(3),
    STRING_VAL(4),
    BOOL_VAL(5),
    LIST_UINT32_VAL(6),
    VALUE_NOT_SET(0);
    private final int value;
    private ValueCase(int value) {
      this.value = value;
    }
    /**
     * @deprecated Use {@link #forNumber(int)} instead.
     */
    @java.lang.Deprecated
    public static ValueCase valueOf(int value) {
      return forNumber(value);
    }

    public static ValueCase forNumber(int value) {
      switch (value) {
        case 3: return INT64_VAL;
        case 4: return STRING_VAL;
        case 5: return BOOL_VAL;
        case 6: return LIST_UINT32_VAL;
        case 0: return VALUE_NOT_SET;
        default: return null;
      }
    }
    public int getNumber() {
      return this.value;
    }
  };

  public ValueCase
  getValueCase() {
    return ValueCase.forNumber(
        valueCase_);
  }

  public static final int KEY_FIELD_NUMBER = 1;
  private volatile java.lang.Object key_;
  /**
   * <code>string key = 1;</code>
   */
  public java.lang.String getKey() {
    java.lang.Object ref = key_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      key_ = s;
      return s;
    }
  }
  /**
   * <code>string key = 1;</code>
   */
  public com.google.protobuf.ByteString
      getKeyBytes() {
    java.lang.Object ref = key_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      key_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int METHOD_FIELD_NUMBER = 2;
  private volatile java.lang.Object method_;
  /**
   * <code>string method = 2;</code>
   */
  public java.lang.String getMethod() {
    java.lang.Object ref = method_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      method_ = s;
      return s;
    }
  }
  /**
   * <code>string method = 2;</code>
   */
  public com.google.protobuf.ByteString
      getMethodBytes() {
    java.lang.Object ref = method_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      method_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int INT64_VAL_FIELD_NUMBER = 3;
  /**
   * <code>int64 int64_val = 3;</code>
   */
  public long getInt64Val() {
    if (valueCase_ == 3) {
      return (java.lang.Long) value_;
    }
    return 0L;
  }

  public static final int STRING_VAL_FIELD_NUMBER = 4;
  /**
   * <code>string string_val = 4;</code>
   */
  public java.lang.String getStringVal() {
    java.lang.Object ref = "";
    if (valueCase_ == 4) {
      ref = value_;
    }
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      if (valueCase_ == 4) {
        value_ = s;
      }
      return s;
    }
  }
  /**
   * <code>string string_val = 4;</code>
   */
  public com.google.protobuf.ByteString
      getStringValBytes() {
    java.lang.Object ref = "";
    if (valueCase_ == 4) {
      ref = value_;
    }
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      if (valueCase_ == 4) {
        value_ = b;
      }
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int BOOL_VAL_FIELD_NUMBER = 5;
  /**
   * <code>bool bool_val = 5;</code>
   */
  public boolean getBoolVal() {
    if (valueCase_ == 5) {
      return (java.lang.Boolean) value_;
    }
    return false;
  }

  public static final int LIST_UINT32_VAL_FIELD_NUMBER = 6;
  /**
   * <code>.common.ListUInt32 list_uint32_val = 6;</code>
   */
  public boolean hasListUint32Val() {
    return valueCase_ == 6;
  }
  /**
   * <code>.common.ListUInt32 list_uint32_val = 6;</code>
   */
  public vn.com.tpbfico.grpc.common.ListUInt32 getListUint32Val() {
    if (valueCase_ == 6) {
       return (vn.com.tpbfico.grpc.common.ListUInt32) value_;
    }
    return vn.com.tpbfico.grpc.common.ListUInt32.getDefaultInstance();
  }
  /**
   * <code>.common.ListUInt32 list_uint32_val = 6;</code>
   */
  public vn.com.tpbfico.grpc.common.ListUInt32OrBuilder getListUint32ValOrBuilder() {
    if (valueCase_ == 6) {
       return (vn.com.tpbfico.grpc.common.ListUInt32) value_;
    }
    return vn.com.tpbfico.grpc.common.ListUInt32.getDefaultInstance();
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (!getKeyBytes().isEmpty()) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 1, key_);
    }
    if (!getMethodBytes().isEmpty()) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 2, method_);
    }
    if (valueCase_ == 3) {
      output.writeInt64(
          3, (long)((java.lang.Long) value_));
    }
    if (valueCase_ == 4) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 4, value_);
    }
    if (valueCase_ == 5) {
      output.writeBool(
          5, (boolean)((java.lang.Boolean) value_));
    }
    if (valueCase_ == 6) {
      output.writeMessage(6, (vn.com.tpbfico.grpc.common.ListUInt32) value_);
    }
    unknownFields.writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (!getKeyBytes().isEmpty()) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(1, key_);
    }
    if (!getMethodBytes().isEmpty()) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(2, method_);
    }
    if (valueCase_ == 3) {
      size += com.google.protobuf.CodedOutputStream
        .computeInt64Size(
            3, (long)((java.lang.Long) value_));
    }
    if (valueCase_ == 4) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(4, value_);
    }
    if (valueCase_ == 5) {
      size += com.google.protobuf.CodedOutputStream
        .computeBoolSize(
            5, (boolean)((java.lang.Boolean) value_));
    }
    if (valueCase_ == 6) {
      size += com.google.protobuf.CodedOutputStream
        .computeMessageSize(6, (vn.com.tpbfico.grpc.common.ListUInt32) value_);
    }
    size += unknownFields.getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof vn.com.tpbfico.grpc.common.Filter)) {
      return super.equals(obj);
    }
    vn.com.tpbfico.grpc.common.Filter other = (vn.com.tpbfico.grpc.common.Filter) obj;

    boolean result = true;
    result = result && getKey()
        .equals(other.getKey());
    result = result && getMethod()
        .equals(other.getMethod());
    result = result && getValueCase().equals(
        other.getValueCase());
    if (!result) return false;
    switch (valueCase_) {
      case 3:
        result = result && (getInt64Val()
            == other.getInt64Val());
        break;
      case 4:
        result = result && getStringVal()
            .equals(other.getStringVal());
        break;
      case 5:
        result = result && (getBoolVal()
            == other.getBoolVal());
        break;
      case 6:
        result = result && getListUint32Val()
            .equals(other.getListUint32Val());
        break;
      case 0:
      default:
    }
    result = result && unknownFields.equals(other.unknownFields);
    return result;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    hash = (37 * hash) + KEY_FIELD_NUMBER;
    hash = (53 * hash) + getKey().hashCode();
    hash = (37 * hash) + METHOD_FIELD_NUMBER;
    hash = (53 * hash) + getMethod().hashCode();
    switch (valueCase_) {
      case 3:
        hash = (37 * hash) + INT64_VAL_FIELD_NUMBER;
        hash = (53 * hash) + com.google.protobuf.Internal.hashLong(
            getInt64Val());
        break;
      case 4:
        hash = (37 * hash) + STRING_VAL_FIELD_NUMBER;
        hash = (53 * hash) + getStringVal().hashCode();
        break;
      case 5:
        hash = (37 * hash) + BOOL_VAL_FIELD_NUMBER;
        hash = (53 * hash) + com.google.protobuf.Internal.hashBoolean(
            getBoolVal());
        break;
      case 6:
        hash = (37 * hash) + LIST_UINT32_VAL_FIELD_NUMBER;
        hash = (53 * hash) + getListUint32Val().hashCode();
        break;
      case 0:
      default:
    }
    hash = (29 * hash) + unknownFields.hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static vn.com.tpbfico.grpc.common.Filter parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static vn.com.tpbfico.grpc.common.Filter parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static vn.com.tpbfico.grpc.common.Filter parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static vn.com.tpbfico.grpc.common.Filter parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static vn.com.tpbfico.grpc.common.Filter parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static vn.com.tpbfico.grpc.common.Filter parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static vn.com.tpbfico.grpc.common.Filter parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static vn.com.tpbfico.grpc.common.Filter parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static vn.com.tpbfico.grpc.common.Filter parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static vn.com.tpbfico.grpc.common.Filter parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static vn.com.tpbfico.grpc.common.Filter parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static vn.com.tpbfico.grpc.common.Filter parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(vn.com.tpbfico.grpc.common.Filter prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code common.Filter}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:common.Filter)
      vn.com.tpbfico.grpc.common.FilterOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return vn.com.tpbfico.grpc.common.Common.internal_static_common_Filter_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return vn.com.tpbfico.grpc.common.Common.internal_static_common_Filter_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              vn.com.tpbfico.grpc.common.Filter.class, vn.com.tpbfico.grpc.common.Filter.Builder.class);
    }

    // Construct using vn.com.tpbfico.grpc.common.Filter.newBuilder()
    private Builder() {
      maybeForceBuilderInitialization();
    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);
      maybeForceBuilderInitialization();
    }
    private void maybeForceBuilderInitialization() {
      if (com.google.protobuf.GeneratedMessageV3
              .alwaysUseFieldBuilders) {
      }
    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      key_ = "";

      method_ = "";

      valueCase_ = 0;
      value_ = null;
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return vn.com.tpbfico.grpc.common.Common.internal_static_common_Filter_descriptor;
    }

    @java.lang.Override
    public vn.com.tpbfico.grpc.common.Filter getDefaultInstanceForType() {
      return vn.com.tpbfico.grpc.common.Filter.getDefaultInstance();
    }

    @java.lang.Override
    public vn.com.tpbfico.grpc.common.Filter build() {
      vn.com.tpbfico.grpc.common.Filter result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public vn.com.tpbfico.grpc.common.Filter buildPartial() {
      vn.com.tpbfico.grpc.common.Filter result = new vn.com.tpbfico.grpc.common.Filter(this);
      result.key_ = key_;
      result.method_ = method_;
      if (valueCase_ == 3) {
        result.value_ = value_;
      }
      if (valueCase_ == 4) {
        result.value_ = value_;
      }
      if (valueCase_ == 5) {
        result.value_ = value_;
      }
      if (valueCase_ == 6) {
        if (listUint32ValBuilder_ == null) {
          result.value_ = value_;
        } else {
          result.value_ = listUint32ValBuilder_.build();
        }
      }
      result.valueCase_ = valueCase_;
      onBuilt();
      return result;
    }

    @java.lang.Override
    public Builder clone() {
      return (Builder) super.clone();
    }
    @java.lang.Override
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return (Builder) super.setField(field, value);
    }
    @java.lang.Override
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return (Builder) super.clearField(field);
    }
    @java.lang.Override
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return (Builder) super.clearOneof(oneof);
    }
    @java.lang.Override
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, java.lang.Object value) {
      return (Builder) super.setRepeatedField(field, index, value);
    }
    @java.lang.Override
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return (Builder) super.addRepeatedField(field, value);
    }
    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof vn.com.tpbfico.grpc.common.Filter) {
        return mergeFrom((vn.com.tpbfico.grpc.common.Filter)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(vn.com.tpbfico.grpc.common.Filter other) {
      if (other == vn.com.tpbfico.grpc.common.Filter.getDefaultInstance()) return this;
      if (!other.getKey().isEmpty()) {
        key_ = other.key_;
        onChanged();
      }
      if (!other.getMethod().isEmpty()) {
        method_ = other.method_;
        onChanged();
      }
      switch (other.getValueCase()) {
        case INT64_VAL: {
          setInt64Val(other.getInt64Val());
          break;
        }
        case STRING_VAL: {
          valueCase_ = 4;
          value_ = other.value_;
          onChanged();
          break;
        }
        case BOOL_VAL: {
          setBoolVal(other.getBoolVal());
          break;
        }
        case LIST_UINT32_VAL: {
          mergeListUint32Val(other.getListUint32Val());
          break;
        }
        case VALUE_NOT_SET: {
          break;
        }
      }
      this.mergeUnknownFields(other.unknownFields);
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      vn.com.tpbfico.grpc.common.Filter parsedMessage = null;
      try {
        parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        parsedMessage = (vn.com.tpbfico.grpc.common.Filter) e.getUnfinishedMessage();
        throw e.unwrapIOException();
      } finally {
        if (parsedMessage != null) {
          mergeFrom(parsedMessage);
        }
      }
      return this;
    }
    private int valueCase_ = 0;
    private java.lang.Object value_;
    public ValueCase
        getValueCase() {
      return ValueCase.forNumber(
          valueCase_);
    }

    public Builder clearValue() {
      valueCase_ = 0;
      value_ = null;
      onChanged();
      return this;
    }


    private java.lang.Object key_ = "";
    /**
     * <code>string key = 1;</code>
     */
    public java.lang.String getKey() {
      java.lang.Object ref = key_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        key_ = s;
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>string key = 1;</code>
     */
    public com.google.protobuf.ByteString
        getKeyBytes() {
      java.lang.Object ref = key_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        key_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>string key = 1;</code>
     */
    public Builder setKey(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      key_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>string key = 1;</code>
     */
    public Builder clearKey() {
      
      key_ = getDefaultInstance().getKey();
      onChanged();
      return this;
    }
    /**
     * <code>string key = 1;</code>
     */
    public Builder setKeyBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      key_ = value;
      onChanged();
      return this;
    }

    private java.lang.Object method_ = "";
    /**
     * <code>string method = 2;</code>
     */
    public java.lang.String getMethod() {
      java.lang.Object ref = method_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        method_ = s;
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>string method = 2;</code>
     */
    public com.google.protobuf.ByteString
        getMethodBytes() {
      java.lang.Object ref = method_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        method_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>string method = 2;</code>
     */
    public Builder setMethod(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  
      method_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>string method = 2;</code>
     */
    public Builder clearMethod() {
      
      method_ = getDefaultInstance().getMethod();
      onChanged();
      return this;
    }
    /**
     * <code>string method = 2;</code>
     */
    public Builder setMethodBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      
      method_ = value;
      onChanged();
      return this;
    }

    /**
     * <code>int64 int64_val = 3;</code>
     */
    public long getInt64Val() {
      if (valueCase_ == 3) {
        return (java.lang.Long) value_;
      }
      return 0L;
    }
    /**
     * <code>int64 int64_val = 3;</code>
     */
    public Builder setInt64Val(long value) {
      valueCase_ = 3;
      value_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>int64 int64_val = 3;</code>
     */
    public Builder clearInt64Val() {
      if (valueCase_ == 3) {
        valueCase_ = 0;
        value_ = null;
        onChanged();
      }
      return this;
    }

    /**
     * <code>string string_val = 4;</code>
     */
    public java.lang.String getStringVal() {
      java.lang.Object ref = "";
      if (valueCase_ == 4) {
        ref = value_;
      }
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        if (valueCase_ == 4) {
          value_ = s;
        }
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>string string_val = 4;</code>
     */
    public com.google.protobuf.ByteString
        getStringValBytes() {
      java.lang.Object ref = "";
      if (valueCase_ == 4) {
        ref = value_;
      }
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        if (valueCase_ == 4) {
          value_ = b;
        }
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>string string_val = 4;</code>
     */
    public Builder setStringVal(
        java.lang.String value) {
      if (value == null) {
    throw new NullPointerException();
  }
  valueCase_ = 4;
      value_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>string string_val = 4;</code>
     */
    public Builder clearStringVal() {
      if (valueCase_ == 4) {
        valueCase_ = 0;
        value_ = null;
        onChanged();
      }
      return this;
    }
    /**
     * <code>string string_val = 4;</code>
     */
    public Builder setStringValBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
      valueCase_ = 4;
      value_ = value;
      onChanged();
      return this;
    }

    /**
     * <code>bool bool_val = 5;</code>
     */
    public boolean getBoolVal() {
      if (valueCase_ == 5) {
        return (java.lang.Boolean) value_;
      }
      return false;
    }
    /**
     * <code>bool bool_val = 5;</code>
     */
    public Builder setBoolVal(boolean value) {
      valueCase_ = 5;
      value_ = value;
      onChanged();
      return this;
    }
    /**
     * <code>bool bool_val = 5;</code>
     */
    public Builder clearBoolVal() {
      if (valueCase_ == 5) {
        valueCase_ = 0;
        value_ = null;
        onChanged();
      }
      return this;
    }

    private com.google.protobuf.SingleFieldBuilderV3<
        vn.com.tpbfico.grpc.common.ListUInt32, vn.com.tpbfico.grpc.common.ListUInt32.Builder, vn.com.tpbfico.grpc.common.ListUInt32OrBuilder> listUint32ValBuilder_;
    /**
     * <code>.common.ListUInt32 list_uint32_val = 6;</code>
     */
    public boolean hasListUint32Val() {
      return valueCase_ == 6;
    }
    /**
     * <code>.common.ListUInt32 list_uint32_val = 6;</code>
     */
    public vn.com.tpbfico.grpc.common.ListUInt32 getListUint32Val() {
      if (listUint32ValBuilder_ == null) {
        if (valueCase_ == 6) {
          return (vn.com.tpbfico.grpc.common.ListUInt32) value_;
        }
        return vn.com.tpbfico.grpc.common.ListUInt32.getDefaultInstance();
      } else {
        if (valueCase_ == 6) {
          return listUint32ValBuilder_.getMessage();
        }
        return vn.com.tpbfico.grpc.common.ListUInt32.getDefaultInstance();
      }
    }
    /**
     * <code>.common.ListUInt32 list_uint32_val = 6;</code>
     */
    public Builder setListUint32Val(vn.com.tpbfico.grpc.common.ListUInt32 value) {
      if (listUint32ValBuilder_ == null) {
        if (value == null) {
          throw new NullPointerException();
        }
        value_ = value;
        onChanged();
      } else {
        listUint32ValBuilder_.setMessage(value);
      }
      valueCase_ = 6;
      return this;
    }
    /**
     * <code>.common.ListUInt32 list_uint32_val = 6;</code>
     */
    public Builder setListUint32Val(
        vn.com.tpbfico.grpc.common.ListUInt32.Builder builderForValue) {
      if (listUint32ValBuilder_ == null) {
        value_ = builderForValue.build();
        onChanged();
      } else {
        listUint32ValBuilder_.setMessage(builderForValue.build());
      }
      valueCase_ = 6;
      return this;
    }
    /**
     * <code>.common.ListUInt32 list_uint32_val = 6;</code>
     */
    public Builder mergeListUint32Val(vn.com.tpbfico.grpc.common.ListUInt32 value) {
      if (listUint32ValBuilder_ == null) {
        if (valueCase_ == 6 &&
            value_ != vn.com.tpbfico.grpc.common.ListUInt32.getDefaultInstance()) {
          value_ = vn.com.tpbfico.grpc.common.ListUInt32.newBuilder((vn.com.tpbfico.grpc.common.ListUInt32) value_)
              .mergeFrom(value).buildPartial();
        } else {
          value_ = value;
        }
        onChanged();
      } else {
        if (valueCase_ == 6) {
          listUint32ValBuilder_.mergeFrom(value);
        }
        listUint32ValBuilder_.setMessage(value);
      }
      valueCase_ = 6;
      return this;
    }
    /**
     * <code>.common.ListUInt32 list_uint32_val = 6;</code>
     */
    public Builder clearListUint32Val() {
      if (listUint32ValBuilder_ == null) {
        if (valueCase_ == 6) {
          valueCase_ = 0;
          value_ = null;
          onChanged();
        }
      } else {
        if (valueCase_ == 6) {
          valueCase_ = 0;
          value_ = null;
        }
        listUint32ValBuilder_.clear();
      }
      return this;
    }
    /**
     * <code>.common.ListUInt32 list_uint32_val = 6;</code>
     */
    public vn.com.tpbfico.grpc.common.ListUInt32.Builder getListUint32ValBuilder() {
      return getListUint32ValFieldBuilder().getBuilder();
    }
    /**
     * <code>.common.ListUInt32 list_uint32_val = 6;</code>
     */
    public vn.com.tpbfico.grpc.common.ListUInt32OrBuilder getListUint32ValOrBuilder() {
      if ((valueCase_ == 6) && (listUint32ValBuilder_ != null)) {
        return listUint32ValBuilder_.getMessageOrBuilder();
      } else {
        if (valueCase_ == 6) {
          return (vn.com.tpbfico.grpc.common.ListUInt32) value_;
        }
        return vn.com.tpbfico.grpc.common.ListUInt32.getDefaultInstance();
      }
    }
    /**
     * <code>.common.ListUInt32 list_uint32_val = 6;</code>
     */
    private com.google.protobuf.SingleFieldBuilderV3<
        vn.com.tpbfico.grpc.common.ListUInt32, vn.com.tpbfico.grpc.common.ListUInt32.Builder, vn.com.tpbfico.grpc.common.ListUInt32OrBuilder> 
        getListUint32ValFieldBuilder() {
      if (listUint32ValBuilder_ == null) {
        if (!(valueCase_ == 6)) {
          value_ = vn.com.tpbfico.grpc.common.ListUInt32.getDefaultInstance();
        }
        listUint32ValBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
            vn.com.tpbfico.grpc.common.ListUInt32, vn.com.tpbfico.grpc.common.ListUInt32.Builder, vn.com.tpbfico.grpc.common.ListUInt32OrBuilder>(
                (vn.com.tpbfico.grpc.common.ListUInt32) value_,
                getParentForChildren(),
                isClean());
        value_ = null;
      }
      valueCase_ = 6;
      onChanged();;
      return listUint32ValBuilder_;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFieldsProto3(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:common.Filter)
  }

  // @@protoc_insertion_point(class_scope:common.Filter)
  private static final vn.com.tpbfico.grpc.common.Filter DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new vn.com.tpbfico.grpc.common.Filter();
  }

  public static vn.com.tpbfico.grpc.common.Filter getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<Filter>
      PARSER = new com.google.protobuf.AbstractParser<Filter>() {
    @java.lang.Override
    public Filter parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return new Filter(input, extensionRegistry);
    }
  };

  public static com.google.protobuf.Parser<Filter> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<Filter> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public vn.com.tpbfico.grpc.common.Filter getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

