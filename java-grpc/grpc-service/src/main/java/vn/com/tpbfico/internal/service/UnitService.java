package vn.com.tpbfico.internal.service;

import com.google.rpc.Code;
import com.google.rpc.Status;
import io.grpc.protobuf.StatusProto;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;
import vn.com.tpbfico.internal.builder.UnitBuilder;
import vn.com.tpbfico.internal.entity.Units;
import vn.com.tpbfico.grpc.*;
import vn.com.tpbfico.internal.repository.UnitsRepository;

import java.util.Optional;

@GrpcService
public class UnitService extends UnitServiceGrpc.UnitServiceImplBase {

    @Autowired
    private UnitsRepository unitsRepository;

    @Override
    public void getUnit(GetUnitRequest request, StreamObserver<Unit> responseObserver) {
        int id = request.getId();
        try {
//            Units u = new Units();//unitsRepository.findById(id).get();
            Optional optional = unitsRepository.findById(id);
            Units u = (Units) optional.get();
            UnitBuilder unitBuilder = UnitBuilder.getInstance();
            Unit unitProto = Unit.newBuilder()
                    .setUnitGroup(unitBuilder.toUnitGroupProto(u.getUnitGroups()))
                    .setCode(u.getCode())
                    .setName(u.getName())
                    .setId(u.getId())
                    .setDescription(u.getDescription())
                    .setUnitGroupId(Math.toIntExact(u.getUnitGroups().getId()))
                    .setEditedBy(u.getEditedBy())
                    .setIsActive(u.getActive())
                    .build();
            responseObserver.onNext(unitProto);
            responseObserver.onCompleted();
        } catch (Exception ex) {
            Status status = Status.newBuilder()
                    .setCode(Code.INTERNAL_VALUE)
                    .setMessage(ex.getMessage())
                    .build();
            responseObserver.onError(StatusProto.toStatusRuntimeException(status));
        }
    }

    @Override
    public void updateUnit(UpdateUnitRequest request, StreamObserver<Unit> responseObserver) {
        super.updateUnit(request, responseObserver);
    }

    @Override
    public void createUnit(CreateUnitRequest request, StreamObserver<Unit> responseObserver) {
        super.createUnit(request, responseObserver);
    }

    @Override
    public void createUnitGroup(CreateUnitGroupRequest request, StreamObserver<UnitGroup> responseObserver) {
        super.createUnitGroup(request, responseObserver);
    }

    @Override
    public void listUnitGroup(ListUnitGroupRequest request, StreamObserver<ListUnitGroupResponse> responseObserver) {
        super.listUnitGroup(request, responseObserver);
    }
}
