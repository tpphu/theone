package main

import (
	"log"
	"os"

	"github.com/urfave/cli/v2"
	"theone.io/tools/onekit/protoc"
)

func main() {
	app := &cli.App{
		Commands: []*cli.Command{
			{
				Name:   "protoc",
				Usage:  "Gen proto file",
				Action: protoc.Action,
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
