module theone.io

go 1.16

require (
	github.com/envoyproxy/protoc-gen-validate v0.4.1
	github.com/grpc-ecosystem/go-grpc-middleware v1.0.0
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.6.0
	github.com/labstack/gommon v0.3.0
	github.com/slack-go/slack v0.9.5
	github.com/spf13/cobra v1.1.3
	github.com/urfave/cli/v2 v2.3.0
	go.uber.org/dig v1.10.0
	google.golang.org/genproto v0.0.0-20210903162649-d08c68adba83
	google.golang.org/grpc v1.40.0
	google.golang.org/protobuf v1.27.1
)
