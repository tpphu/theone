package iam

type Key string

const (
	Identity Key = "Identity"
)

type IUser interface {
	GetId() uint
	GetName() string
}

type User struct {
	Id   uint
	Name string
}

func (u *User) GetName() string {
	if u == nil {
		return ""
	}
	return u.Name
}

func (u *User) GetId() uint {
	if u == nil {
		return 0
	}
	return u.Id
}
