package grpc

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
	"go.uber.org/dig"
	"theone.io/backend/core/internal/services"
)

type Server struct {
	// shutdownReason     string
	// shutdownInProgress bool
	container *dig.Container
}

var ServiceCmd = &cobra.Command{
	Use:   "app",
	Short: "theone.io GRPC APIs",
	Long:  `theone.io GRPC APIs`,
	Run: func(cmd *cobra.Command, args []string) {
		err := NewServer().Run()
		if err != nil {
			panic(err)
		}
		fmt.Println("exit")
	},
	Version: "1.0.0",
}

func NewServer() *Server {
	container := dig.New()

	return &Server{
		container: container,
	}
}

func (s *Server) Run() error {
	s.addToContainer(
		services.Init,
		Init,
	)
	err := s.container.Invoke(func(server GrpcServer) {
		_ = server.Run(context.Background())
	})
	return err
}

func (s *Server) addToContainer(in ...interface{}) {
	var err error
	for _, i := range in {
		err = s.container.Provide(i)
		if err != nil {
			fmt.Println(err)
		}
	}
}
