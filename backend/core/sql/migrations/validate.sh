#!/bin/sh

for f in *.sql ; do 
  set -o pipefail
  cat $f | soar -only-syntax-check
  RESULT=$?
  if [ $RESULT -eq 0 ]; then
    echo "$f" is success
  else
    echo "$f" is error
    exit $RESULT
  fi
done